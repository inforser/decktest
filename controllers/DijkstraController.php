<?php

namespace app\controllers;

use app\models\Edge;
use app\models\Node;
use yii\web\Controller;


class DijkstraController extends Controller
{
    public function actionDijkstra($from, $to)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if(Node::find()->where( [ 'id' => $from ] )->exists() && Node::find()->where( [ 'id' => $to ] )->exists())
        {
            $d_nodes = [];
            $nodes = Node::find()->all();
            foreach ($nodes as $node)
            {
                $d_nodes[$node->id] = [
                    'weight' => PHP_INT_MAX,
                    'viewed' => false,
                    'route' => [],
                ];
            }
            $d_nodes[$from]['weight'] = 0;
            $d_nodes[$from]['route'][] = (int)$from;

            $current_node = $from;
            while($current_node>0)
            {
                $edges = Edge::find()->where(['from_node_id' => $current_node])->all();
                foreach ($edges as $edge) {
                    if($d_nodes[$current_node]['weight'] + $edge->weight < $d_nodes[$edge->to_node_id]['weight'])
                    {
                        $d_nodes[$edge->to_node_id]['weight'] = $d_nodes[$current_node]['weight'] + $edge->weight;
                        $d_nodes[$edge->to_node_id]['route'] = $d_nodes[$current_node]['route'];
                        $d_nodes[$edge->to_node_id]['route'][] = $edge->to_node_id;
                    }
                }
                $d_nodes[$current_node]['viewed'] = true;
                $current_node = $this->nextNode($d_nodes);
            }

            return $d_nodes[$to]['route'];

        } else {
            return [
                'error' =>
                [
                    'error_msg' => 'This nodes doesn\'t exist!'
                ]
            ];
        }
    }

    private function nextNode($d_nodes) {
        $best_node = -1;
        $best_weight = PHP_INT_MAX;
        foreach ($d_nodes as $id => $node) {
            if(!$node['viewed'] && $node['weight']<$best_weight) {
                $best_weight = $node['weight'];
                $best_node = $id;
            }
        }
        return $best_node;
    }
}