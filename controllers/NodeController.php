<?php

namespace app\controllers;

use yii\rest\ActiveController;
use Yii;
use app\models\Node;
use app\models\Edge;

class NodeController extends ActiveController
{
    public $modelClass = 'app\models\Node';

    public function actionDelete($id)
    {
        Edge::deleteAll('from_node_id = :node OR to_node_id = :node', [':node' => $id]);

        $node = Node::findOne($id);

        if ($node->delete() === false) {
            throw new ServerErrorHttpException('Failed to delete the object for unknown reason.');
        }

        Yii::$app->getResponse()->setStatusCode(204);
    }
}