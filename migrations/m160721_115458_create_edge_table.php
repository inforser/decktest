<?php

use yii\db\Migration;

/**
 * Handles the creation for table `edge`.
 * Has foreign keys to the tables:
 *
 * - `node`
 * - `node`
 */
class m160721_115458_create_edge_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('edge', [
            'id' => $this->primaryKey(),
            'from_node_id' => $this->integer()->notNull(),
            'to_node_id' => $this->integer()->notNull(),
            'weight' => $this->integer(),
        ]);

        // creates index for column `from_node_id`
        $this->createIndex(
            'idx-edge-from_node_id',
            'edge',
            'from_node_id'
        );

        // add foreign key for table `node`
        $this->addForeignKey(
            'fk-edge-from_node_id',
            'edge',
            'from_node_id',
            'node',
            'id',
            'CASCADE'
        );

        // creates index for column `to_node_id`
        $this->createIndex(
            'idx-edge-to_node_id',
            'edge',
            'to_node_id'
        );

        // add foreign key for table `node`
        $this->addForeignKey(
            'fk-edge-to_node_id',
            'edge',
            'to_node_id',
            'node',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `node`
        $this->dropForeignKey(
            'fk-edge-from_node_id',
            'edge'
        );

        // drops index for column `from_node_id`
        $this->dropIndex(
            'idx-edge-from_node_id',
            'edge'
        );

        // drops foreign key for table `node`
        $this->dropForeignKey(
            'fk-edge-to_node_id',
            'edge'
        );

        // drops index for column `to_node_id`
        $this->dropIndex(
            'idx-edge-to_node_id',
            'edge'
        );

        $this->dropTable('edge');
    }
}
