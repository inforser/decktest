<?php

use yii\db\Migration;

/**
 * Handles the creation for table `node`.
 */
class m160721_114138_create_node_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('node', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('node');
    }
}
