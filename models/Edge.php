<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "edge".
 *
 * @property integer $id
 * @property integer $from_node_id
 * @property integer $to_node_id
 * @property integer $weight
 *
 * @property Node $toNode
 * @property Node $fromNode
 */
class Edge extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'edge';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from_node_id', 'to_node_id'], 'required'],
            [['from_node_id', 'to_node_id', 'weight'], 'integer'],
            [['to_node_id'], 'exist', 'skipOnError' => true, 'targetClass' => Node::className(), 'targetAttribute' => ['to_node_id' => 'id']],
            [['from_node_id'], 'exist', 'skipOnError' => true, 'targetClass' => Node::className(), 'targetAttribute' => ['from_node_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'from_node_id' => 'From Node ID',
            'to_node_id' => 'To Node ID',
            'weight' => 'Weight',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToNode()
    {
        return $this->hasOne(Node::className(), ['id' => 'to_node_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFromNode()
    {
        return $this->hasOne(Node::className(), ['id' => 'from_node_id']);
    }
}
